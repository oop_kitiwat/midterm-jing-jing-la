package com.kiyzzy.mid;

public class Boss {
    private String name;
    private int hpboss;
    
    private int x;
    public static final int MIN_X = 0;
    public static final int MAX_X = 20;

    public Boss(String name,int hpboss,int x){
        this.name = name;
        
        this.hpboss = hpboss;
        this.x = x;
        
    }
    public String getName() {
        return name;
    }
    public int getHpBoss() {
        return hpboss;
    }
    
    
    public int getX() {
        return x;
    }
    public void setName(String name) {
        this.name = name;
    }
    public boolean left() {
        if(x == MIN_X)
            return false;
        x = x-1;
        return true;
    }
    public boolean leftStep(int step) {
        for (int i = 0; i < step; i++) {
            if (!left()) {
                return false;
            }
        } return true; 
    }
    public boolean right() {
        if(x == MAX_X)
            return false;
        x = x +1;
        return true;
    }
    public boolean rightStep(int step) {
        for (int i = 0; i < step; i++) {
            if (!right()) {
                return false;
            }
        } return true; 
    }

    public void print() {
        System.out.println("The "+name);
        System.out.println("Health Point: "+hpboss);
        System.out.println("Position"+" X:" + x);
    }



    
    
}
