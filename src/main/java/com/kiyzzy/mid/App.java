package com.kiyzzy.mid;

public class App {
    public static void main(String[] args) {
        Boss slime = new Boss("King Slime", 2800, 0); // 1
        slime.print();
        System.out.println("====================");

        for (int i = 0; i < 5; i++) {
            slime.right();
        }
        slime.print();
        System.out.println("====================");
        Boss brain = new Boss("Brain of Cthulhu", 1700, 10); // 2
        brain.print();
        System.out.println("====================");

        for (int i = 0; i < 9; i++) {
            brain.left();
        }
        brain.print();
        System.out.println("====================");
        Boss deer = new Boss("Deerclops", 14000, 20); // 3
        deer.print();
        System.out.println("====================");
        for (int i = 0; i < 15; i++) {
            deer.left();
            
            }
            deer.print();
            System.out.println("====================");
    }
}
