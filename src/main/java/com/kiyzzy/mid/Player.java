package com.kiyzzy.mid;



public class Player {
    private String name;
    private String job;
    private int level = 1;
    
    private int atk = 5;
    private int def = 7;

    public Player(String name,String job,int level) {
        this.name = name;
        this.job = job;
        this.level = level;
        
    }
    public String getName() {
        return name;
    }
    public String getJob() {
        return job;
    }
    public int getAtk() {
        return atk;
    }
    public int getLevel() {
        return level;
    }
    public int getDef() {
        return def;
    }
    public boolean levelup() {
        if(level < 1) return false;
            level = level + 1;
        return true;
    }
    public int atkUp() {
        if(level >= 1)
        atk = atk + 2;
        return atk;
    }
    public int defUp() {
        if(level >= 1) 
        def = def + 2;
        return def;
    }
    public void status(){
        System.out.println("Name: "+name);
        System.out.println("Job: " + job);
        System.out.println("Level: " + level);
        System.out.println("Attack: "+ atk);
        System.out.println("Defense: " + def);
        }



    
}
